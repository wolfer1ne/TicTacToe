﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace TicTacToe
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private MarkType[] gameBoard;
		private bool player1Turn;
		private bool gameFinished;
		
		public MainWindow()
		{
			InitializeComponent();

			resetButtons();
			NewGame();
		}

		private void resetButtons()
		{
			GameBoard.Children.Cast<Button>().ToList().ForEach(btn =>
			{
				btn.Content = string.Empty;
				btn.Foreground = Brushes.Blue;
				btn.Background = Brushes.White;
			});
		}

		private void RestartGame()
		{
			resetButtons();
			NewGame();
		}

		private void NewGame()
		{
			gameBoard = new MarkType[9];

			for (int i = 0; i < gameBoard.Length; i++)
			{
				gameBoard[i] = MarkType.Free;
			}

			player1Turn = true;
			gameFinished = false;
		}

		private void btnReset_Click(object sender, RoutedEventArgs e)
		{
			RestartGame();
		}

		private void btnClick(object sender, RoutedEventArgs e)
		{
			if (gameFinished)
			{
				RestartGame();
				return;
			}

			var button = (Button) sender;

			var column = Grid.GetColumn(button);
			var row = Grid.GetRow(button);

			int index = column + (row * 3);

			if (gameBoard[index] != MarkType.Free)
			{
				return;
			}

			if (!player1Turn)
			{
				button.Foreground = Brushes.Red;
			}

			gameBoard[index] = player1Turn ? MarkType.Cross : MarkType.Circle;
			button.Content = player1Turn ? "X" : "O";

			player1Turn = !player1Turn;

			CheckBoard();
		}

		private int calculateRowStart(int row, int boardSize)
		{
			return row * boardSize;
		}

		private Boolean checkRow(int row, int boardSize)
		{
			int rowIndex = calculateRowStart(row, boardSize);
			ArraySegment<MarkType> rowSegment = new ArraySegment<MarkType>(gameBoard, rowIndex, boardSize);
			if (rowSegment.All(o => o == rowSegment.ToArray()[0] && o != MarkType.Free))
			{
				return true;
			}
			return false;
		}

		private Boolean checkColumn(int col, int boardSize)
		{
			List<MarkType> column = new List<MarkType>();
			for (int i = 0; i < boardSize; i++)
			{
				column.Add(gameBoard[i * boardSize + col]);
			}

			if (column.All(o => o == column.ToArray()[0] && o != MarkType.Free))
			{
				return true;
			}
			return false;
		}

		private Boolean checkDiagonal(int boardSize)
		{
			MarkType mark = gameBoard[0];

			if (mark == MarkType.Free)
			{
				return false;
			}

			for (int i = 0; i < boardSize; i++)
			{
				if (mark != gameBoard[i * boardSize + i])
				{
					return false;
				}
			}

			return true;
		}

		private Boolean check2Diagonal(int boardSize)
		{
			MarkType mark = gameBoard[boardSize-1];

			if (mark == MarkType.Free)
			{
				return false;
			}

			for (int i = 0; i < boardSize; i++)
			{
				if (mark != gameBoard[(boardSize * i - i) + (boardSize -1)])
				{
					return false;
				}
			}

			return true;
		}

		private bool checkMark(MarkType mark)
		{
			if (mark == MarkType.Free)
			{
				return false;
			}

			return true;
		}

		private void printWinner(MarkType mark)
		{
			if (mark == MarkType.Cross)
			{
				MessageBox.Show("Player 1 won!",
						"Winner",
						MessageBoxButton.OK,
						MessageBoxImage.Information);
			}
			else
			{
				MessageBox.Show("Player 2 won!",
						"Winner",
						MessageBoxButton.OK,
						MessageBoxImage.Information);
			}
		}

		// Fix hardcoded size
		private void CheckBoard()
		{
			for (int i = 0; i < 3; i++)
			{
				MarkType mark = MarkType.Free;
				if (checkRow(i, 3))
				{
					mark = gameBoard[calculateRowStart(i, 3)];
				}

				if (checkMark(mark))
				{
					gameFinished = true;
					hlRow(i, 3);
					printWinner(mark);
					return;
				}

				if (checkColumn(i, 3))
				{
					mark = gameBoard[i];
				}

				if (checkMark(mark))
				{
					gameFinished = true;
					hlColumn(i, 3);
					printWinner(mark);
					return;
				}
			}

			if (checkDiagonal(3))
			{
				hlDiagonal(3);
				MarkType mark = gameBoard[0];
				printWinner(mark);
				gameFinished = true;
				return;
			}

			if (check2Diagonal(3))
			{
				hlDiagonal2(3);
				MarkType mark = gameBoard[2];
				printWinner(mark);
				gameFinished = true;
				return;
			}

			// Tie
			if (gameBoard.All(o => o != MarkType.Free))
			{
				MessageBox.Show("It's a tie :(",
						"Game finished",
						MessageBoxButton.OK,
						MessageBoxImage.Information);

				GameBoard.Children.Cast<Button>().ToList().ForEach(btn =>
				{
					btn.Background = Brushes.LightCyan;
				});
				gameFinished = true;
			}
		}

		private void hlColumn(int col, int boardSize)
		{
			List<Button> btns = GameBoard.Children.Cast<Button>().ToList();

			for (int i = 0; i < boardSize; i++)
			{
				btns[i * boardSize + col].Background = Brushes.Green;
			}
		}

		private void hlRow(int row, int boardSize)
		{
			List<Button> btns = GameBoard.Children.Cast<Button>().ToList();

			int rowIndex = calculateRowStart(row, boardSize);

			for (int i = 0; i < boardSize; i++)
			{
				btns[rowIndex + i].Background = Brushes.Green;
			}
		}

		private void hlDiagonal(int boardSize)
		{
			List<Button> btns = GameBoard.Children.Cast<Button>().ToList();

			for (int i = 0; i < boardSize; i++)
			{
				btns[i * boardSize + i].Background = Brushes.Green;
			}
		}

		private void hlDiagonal2(int boardSize)
		{
			List<Button> btns = GameBoard.Children.Cast<Button>().ToList();

			for (int i = 0; i < boardSize; i++)
			{
				btns[(boardSize * i - i) + (boardSize - 1)].Background = Brushes.Green;
			}
		}
	}
}
